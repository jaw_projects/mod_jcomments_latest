## JComments Latest

Расширение модуля Joomla [JComments Latest](http://www.joomlatune.ru/jcomments-modules.html#mod_jcomments_latest "JComments Latest module")

-   Contributors: pshentsoff
-   [Donate link](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FGRFBSFEW5V3Y "Please, donate to support project")
-   Tags: Joomla, JComments, JComments Latest, module
-   Requires at least:
-   Tested up to: PHP 5.4, Joomla 2.5.28
-   Stable tag:
-   Author: pshentsoff
-   [Author's homepage](http://pshentsoff.ru "Author's homepage")
-   License: GNU/GPL3
-   License URI: http://www.gnu.org/licenses/gpl-3.0.html

### Useful links:

-   [JComments Latest](http://www.joomlatune.ru/jcomments-modules.html#mod_jcomments_latest "JComments Latest module")
-   [Forum Thread](http://joomlaforum.ru/index.php/topic,8204.0.html)

#### Version 1.0.0
#### Last work version 1.0.0
#### Last stable version 1.0.0

### Versions history

#### version 1.0.0
-   Добавлена вариант выборки случайных комментариев
